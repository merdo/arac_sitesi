﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="anasayfa.aspx.cs" Inherits="def.anasayfa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Oto Galeri</title>

    <link href="/stylesheet1.css" rel="stylesheet" />
    <script src="js/jquery-3.2.1.min.js"></script>

    <script type="text/javascript">
        $("document").ready(function(){
        
            $("header nav ul li.konular-wrap").mouseover(function () {

                $("header nav ul li.konular-wrap .konular").css("display","block")
            })


            $("header nav ul li.konular-wrap").mouseleave(function () {

                $("header nav ul li.konular-wrap .konular").css("display", "none")
            })
        })
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
    

        <header>

            <div class="logo">

                LOGO 
            </div>

            <nav>
                <ul>
                    <li>
                        <a href="anasayfa.aspx"> Ana Sayfa </a>
                        
                    </li>
                    
                    <li class="konular-wrap">
                       <a href="#"> Satın al </a>
                        <div class="konular">
                            <ul>
                                <li> programlama</li>
                                <li> webprogramlama</li>
                                <li> dersyok</li>

                            </ul>

                        </div>
                    </li>
                    
                    <li>
                        <a href="anasayfa.aspx"> Kirala </a>
                    </li>
                </ul>

            </nav>

            <div class="arama">
                <div class="aramaSol">
                <asp:Button ID="btnGiris" CssClass="btnGiris" runat="server" Text="Giriş" OnClick="btnGiris_Click" />
                    </div>

                <div class="aramaSag">
                <asp:Button ID="btnKayit1" CssClass="btnKayit1" runat="server" Text="Kayıt Ol" OnClick="btnKayit1_Click" />
                    </div>
            </div>

           

        </header>

        <div class="banner">
            BANNER

        </div>

        <div class="sol">

            <div class="hizli-kayit">
                <div class="ust">
                    HIZLI KAYIT OL ULEN !
                </div>

                <div class="alt">
                    <span style="font-weight: bold">  Şehir : </span>
           <br />
                    <br /> <center>
            <asp:DropDownList ID="DropDownList1" CssClass="textbox" runat="server" Width="197">
                <asp:ListItem Value="0">Hepsi</asp:ListItem>
                <asp:ListItem Value="01">Adana</asp:ListItem>
                <asp:ListItem Value="02">Adıyaman</asp:ListItem>
                <asp:ListItem Value="03">Afyonkarahisar</asp:ListItem>
                <asp:ListItem Value="04">Ağrı</asp:ListItem>
                <asp:ListItem Value="05">Amasya</asp:ListItem>
                <asp:ListItem Value="06">Ankara</asp:ListItem>
                <asp:ListItem Value="07">Antalya</asp:ListItem>
                <asp:ListItem Value="08">Artvin</asp:ListItem>
                <asp:ListItem Value="09">Aydın</asp:ListItem>
                <asp:ListItem Value="10">Balıkesir</asp:ListItem>
                <asp:ListItem Value="11">Bilecik</asp:ListItem>
                <asp:ListItem Value="12">Bingöl</asp:ListItem>
                <asp:ListItem Value="13">Bitlis</asp:ListItem>
                <asp:ListItem Value="14">Bolu</asp:ListItem>
                <asp:ListItem Value="15">Burdur</asp:ListItem>
                <asp:ListItem Value="16">Bursa</asp:ListItem>
                <asp:ListItem Value="17">Çanakkale</asp:ListItem>
                <asp:ListItem Value="18">Çankırı</asp:ListItem>
                <asp:ListItem Value="19">Çorum</asp:ListItem>
                <asp:ListItem Value="20">Denizli</asp:ListItem>
                <asp:ListItem Value="21">Diyarbakır</asp:ListItem>
                <asp:ListItem Value="22">Edirne</asp:ListItem>
                <asp:ListItem Value="23">Elazığ</asp:ListItem>
                <asp:ListItem Value="24">Erzincan</asp:ListItem>
                <asp:ListItem Value="25">Erzurum</asp:ListItem>
                <asp:ListItem Value="26">Eskişehir</asp:ListItem>
                <asp:ListItem Value="27">Gaziantep</asp:ListItem>
                <asp:ListItem Value="28">Giresun</asp:ListItem>
                <asp:ListItem Value="29">Gümüşhane</asp:ListItem>
                <asp:ListItem Value="30">Hakkari</asp:ListItem>
                <asp:ListItem Value="31">Hatay</asp:ListItem>
                <asp:ListItem Value="32">Isparta</asp:ListItem>
                <asp:ListItem Value="33">Mersin</asp:ListItem>
                <asp:ListItem Value="34">İstanbul</asp:ListItem>
                <asp:ListItem Value="35">İzmir</asp:ListItem>
                <asp:ListItem Value="36">Kars</asp:ListItem>
                <asp:ListItem Value="37">Kastamonu</asp:ListItem>
                <asp:ListItem Value="38">Kayseri</asp:ListItem>
                <asp:ListItem Value="39">Kırklareli</asp:ListItem>
                <asp:ListItem Value="40">Kırşehir</asp:ListItem>
                <asp:ListItem Value="41">Kocaeli</asp:ListItem>
                <asp:ListItem Value="42">Konya</asp:ListItem>
                <asp:ListItem Value="43">Kütahya</asp:ListItem>
                <asp:ListItem Value="44">Malatya</asp:ListItem>
                <asp:ListItem Value="45">Manisa</asp:ListItem>
                <asp:ListItem Value="46">Kahramanmaraş</asp:ListItem>
                <asp:ListItem Value="47">Mardin</asp:ListItem>
                <asp:ListItem Value="48">Muğla</asp:ListItem>
                <asp:ListItem Value="49">Muş</asp:ListItem>
                <asp:ListItem Value="50">Nevşehir</asp:ListItem>
                <asp:ListItem Value="51">Niğde</asp:ListItem>
                <asp:ListItem Value="52">Ordu</asp:ListItem>
                <asp:ListItem Value="53">Rize</asp:ListItem>
                <asp:ListItem Value="54">Sakarya</asp:ListItem>
                <asp:ListItem Value="55">Samsun</asp:ListItem>
                <asp:ListItem Value="56">Siirt</asp:ListItem>
                <asp:ListItem Value="57">Sinop</asp:ListItem>
                <asp:ListItem Value="58">Sivas</asp:ListItem>
                <asp:ListItem Value="59">Tekirdağ</asp:ListItem>
                <asp:ListItem Value="60">Tokat</asp:ListItem>
                <asp:ListItem Value="61">Trabzon</asp:ListItem>
                <asp:ListItem Value="62">Tunceli</asp:ListItem>
                <asp:ListItem Value="63">Şanlıurfa</asp:ListItem>
                <asp:ListItem Value="64">Uşak</asp:ListItem>
                <asp:ListItem Value="65">Van</asp:ListItem>
                <asp:ListItem Value="66">Yozgat</asp:ListItem>
                <asp:ListItem Value="67">Zonguldak</asp:ListItem>
                <asp:ListItem Value="68">Aksaray</asp:ListItem>
                <asp:ListItem Value="69">Bayburt</asp:ListItem>
                <asp:ListItem Value="70">Karaman</asp:ListItem>
                <asp:ListItem Value="71">Kırıkkale</asp:ListItem>
                <asp:ListItem Value="72">Batman</asp:ListItem>
                <asp:ListItem Value="73">Şırnak</asp:ListItem>
                <asp:ListItem Value="74">Bartın</asp:ListItem>
                <asp:ListItem Value="75">Ardahan</asp:ListItem>
                <asp:ListItem Value="76">Iğdır</asp:ListItem>
                <asp:ListItem Value="77">Yalova</asp:ListItem>
                <asp:ListItem Value="78">Karabük</asp:ListItem>
                <asp:ListItem Value="79">Kilis</asp:ListItem>
                <asp:ListItem Value="80">Osmaniye</asp:ListItem>
                <asp:ListItem Value="81">Düzce</asp:ListItem>
            </asp:DropDownList>
                    </center>
                     <br />
                    <br />
            <span style="font-weight: bold">Fiyat : </span>
            <br />
            <br />
                    <center>
            <asp:TextBox ID="txtFiyat1" CssClass="textbox" runat="server" Width="90px"></asp:TextBox>
            -
            <asp:TextBox ID="txtFiyat2" CssClass="textbox" runat="server" Width="90px"></asp:TextBox>
                    </center>
            <br />
            <br />
          
            <span style="font-weight: bold">Yıl : </span>
            <br />
            <br />
                    <center>
            <asp:TextBox ID="txtYıl1" CssClass="textbox" runat="server" Width="90px"></asp:TextBox>
            -
            <asp:TextBox ID="txtYıl2" CssClass="textbox" runat="server" Width="90px"></asp:TextBox>
                    </center>
            <br />
            <br />
                    <span style="font-weight: bold">Yakıt : </span>
            <br />
            <br />
                    <center>
            <asp:CheckBoxList ID="chkYakıtList" runat="server">
                <asp:ListItem Value="1">benzin</asp:ListItem>
                <asp:ListItem Value="2">dizel</asp:ListItem>
                <asp:ListItem Value="3">benzin ve LPG</asp:ListItem>
            </asp:CheckBoxList>
                    </center>
            <br />
                    <br />
            <span style="font-weight: bold">Vites : </span>
            <br />
            <br />
                    <center>
            <asp:CheckBoxList ID="chkVitesList" runat="server">
                <asp:ListItem Value="1">otomatik</asp:ListItem>
                <asp:ListItem Value="2">manuel</asp:ListItem>
                <asp:ListItem Value="3">yarıotomatik</asp:ListItem>
            </asp:CheckBoxList>
                    </center>
            <br />
                    <br />
            <span style="font-weight: bold">Km:  </span>
            <br />
            <br />
                    <center>
            <asp:TextBox ID="txtmin" CssClass="textbox" runat="server" Width="90px"></asp:TextBox>
            -
            <asp:TextBox ID="txtmax" CssClass="textbox" runat="server" Width="90px"></asp:TextBox> 
            </center>
                        <br />
                    <br />

                 <center>   <asp:Button Text="Ara" ID="btnKayit" CssClass="btnKayit" runat="server" />
                    </center>
                </div>
            </div>


        </div>

        <div class="icerik">  
          

        </div>

        <footer>
            Bir Seckin  turkmen tasarımıdır.
        </footer>
        
    </div>
    </form>
</body>
</html>
